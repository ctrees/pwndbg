#!/bin/bash
set -ex

# If we are a root in a Docker container and `` doesn't exist
# lets overwrite it with a function that just executes things passed to 
# (yeah it won't work for  executed with flags)


# Helper functions
linux() {
    uname | grep -i Linux &>/dev/null
}
osx() {
    uname | grep -i Darwin &>/dev/null
}

install_apt() {
     apt-get update || true
     apt-get -y install git gdb python3-dev python3-pip python3-setuptools libglib2.0-dev libc6-dbg

    if uname -m | grep x86_64 > /dev/null; then
         apt-get -y install libc6-dbg:i386 || true
    fi
}

install_dnf() {
     dnf update || true
     dnf -y install gdb gdb-gdbserver python-devel python3-devel python-pip python3-pip glib2-devel make
     dnf -y debuginfo-install glibc
}

install_xbps() {
     xbps-install -Su
     xbps-install -Sy gdb gcc python-devel python3-devel python-pip python3-pip glibc-devel make
     xbps-install -Sy glibc-dbg
}

install_swupd() {
     swupd update || true
     swupd bundle-add gdb python3-basic make c-basic
}

install_zypper() {
     zypper refresh || true
     zypper install -y gdb gdbserver python-devel python3-devel python2-pip python3-pip glib2-devel make glibc-debuginfo

    if uname -m | grep x86_64 > /dev/null; then
         zypper install -y glibc-32bit-debuginfo || true
    fi
}

install_emerge() {
    emerge --oneshot --deep --newuse --changed-use --changed-deps dev-lang/python dev-python/pip sys-devel/gdb
}

PYTHON=''
INSTALLFLAGS=''

if osx || [ "$1" == "--user" ]; then
    INSTALLFLAGS="--user"
else
    PYTHON=" "
fi

if linux; then
    distro=$(grep "^ID=" /etc/os-release | cut -d'=' -f2 | sed -e 's/"//g')

    case $distro in
        "ubuntu")
            install_apt
            ;;
        "fedora")
            install_dnf
            ;;
        "clear-linux-os")
            install_swupd
            ;;
        "opensuse-leap")
            install_zypper
            ;;
        "arch")
            echo "Install Arch linux using a community package. See:"
            echo " - https://www.archlinux.org/packages/community/any/pwndbg/"
            echo " - https://aur.archlinux.org/packages/pwndbg-git/"
            exit 1
            ;;
        "manjaro")
            echo "Pwndbg is not avaiable on Manjaro's repositories."
            echo "But it can be installed using Arch's AUR community package. See:"
            echo " - https://www.archlinux.org/packages/community/any/pwndbg/"
            echo " - https://aur.archlinux.org/packages/pwndbg-git/"
            exit 1
            ;;
        "void")
            install_xbps
            ;;
        "gentoo")
            install_emerge

            ;;
        *) # we can add more install command for each distros.
            echo "\"$distro\" is not supported distro. Will search for 'apt' or 'dnf' package managers."
            if hash apt; then
                install_apt
            elif hash dnf; then
                install_dnf
            else
                echo "\"$distro\" is not supported and your distro don't have apt or dnf that we support currently."
                exit
            fi
            ;;
    esac
fi

if ! hash gdb; then
    echo 'Could not find gdb in $PATH'
    exit
fi

# Update all submodules
git submodule update --init --recursive

# Find the Python version used by GDB.
PYVER=$(gdb -batch -q --nx -ex 'pi import platform; print(".".join(platform.python_version_tuple()[:2]))')
PYTHON+=$(gdb -batch -q --nx -ex 'pi import sys; print(sys.executable)')
PYTHON+="${PYVER}"

# Find the Python site-packages that we need to use so that
# GDB can find the files once we've installed them.
if linux && [ -z "$INSTALLFLAGS" ]; then
    SITE_PACKAGES=$(gdb -batch -q --nx -ex 'pi import site; print(site.getsitepackages()[0])')
    INSTALLFLAGS="--target ${SITE_PACKAGES}"
fi

# Make sure that pip is available
if ! ${PYTHON} -m pip -V; then
    ${PYTHON} -m ensurepip ${INSTALLFLAGS} --upgrade
fi

# Upgrade pip itself
${PYTHON} -m pip install ${INSTALLFLAGS} --upgrade pip -i https://pypi.tuna.tsinghua.edu.cn/simple

# Install Python dependencies
${PYTHON} -m pip install ${INSTALLFLAGS} -Ur requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

